package org.binar.chapter4.repository;

import org.binar.chapter4.model.Film;
import org.binar.chapter4.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
}
