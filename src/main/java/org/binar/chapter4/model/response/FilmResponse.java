package org.binar.chapter4.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmResponse {
    private String title;

    private String schedule;

    private Boolean sedangTayang;
}
