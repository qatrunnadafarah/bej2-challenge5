package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class UserActive {

    // nomor_hape
    // NOMOR_HAPE
    // nomorHape

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userID;

    @Column(name = "name")
    private String username;

    private String email;

    private String password;

    @Transient
    private boolean state; //gaakan muncul di database

}
