package org.binar.chapter4.model.request;

import io.swagger.v3.oas.annotations.media.ExampleObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmRequest {

    private String title;

    private String schedule;

    private Boolean sedangTayang;
}
