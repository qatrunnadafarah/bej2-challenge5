package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Seat {

    @EmbeddedId
    private SeatId seatID;

    private String studioName;

}
