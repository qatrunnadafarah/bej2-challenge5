package org.binar.chapter4.service;

import org.binar.chapter4.model.Film;
import org.binar.chapter4.model.response.FilmResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {

    void addFilm (Film film) throws Exception;

    void updateFilm(String title, Integer filmID) throws Exception;

    void deleteFilm(Integer film);

    List<Film> getAllFilms();

    List<FilmResponse> getScheduleByFilmId(String filmId);

    Film searchFilmBySchedule(Integer schedule);

    Film searchFilmByID(Integer filmID);

    Film searchFilmByTitle(String title);
}
