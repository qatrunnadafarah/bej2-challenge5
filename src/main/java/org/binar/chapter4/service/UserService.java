package org.binar.chapter4.service;

import org.binar.chapter4.model.UserActive;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    void addUser(UserActive userActive);

    void updateUser(String username, String email, String password) throws Exception;

    void deleteUserById(Integer userActive) throws Exception;
}
