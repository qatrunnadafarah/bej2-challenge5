package org.binar.chapter4.controller;

import org.binar.chapter4.model.Film;
import org.binar.chapter4.model.UserActive;
import org.binar.chapter4.model.request.FilmRequest;
import org.binar.chapter4.model.request.UserRequest;
import org.binar.chapter4.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    //Menambahkan User
    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody UserRequest userRequest) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "New User Created");

        try {
            UserActive userActive = new UserActive();
            userActive.setUsername(userRequest.getUsername());
            userActive.setPassword(userRequest.getPassword());
            userActive.setEmail(userRequest.getEmail());
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Creating New User failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Mengupdate user
    @PutMapping(value = "/{userID}")
    public ResponseEntity updateUser(@RequestBody UserActive userRequest,
                                     @PathVariable("userID") Integer userID) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "User Updated");

        try {
            String username = userRequest.getUsername();
            String email = userRequest.getEmail();
            String password = userRequest.getPassword();
            userService.updateUser(username, email, password);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Updating user failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }

    //Menghapus User
    @DeleteMapping(value = "/{userID}")
    public ResponseEntity deleteUser(@PathVariable("userID") Integer userID) {
        Map<String, Object> resp = new HashMap<>();
        resp.put("message", "User Deleted");

        try {
            userService.deleteUserById(userID);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "Deleting film failed!." + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.BAD_GATEWAY);
        }
    }
}
