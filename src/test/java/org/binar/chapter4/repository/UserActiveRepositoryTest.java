package org.binar.chapter4.repository;

import org.binar.chapter4.model.UserActive;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserActiveRepositoryTest {
    @Autowired
    UserActiveRepository userActiveRepository;

    // insert data ke database
    @Test
    void testAddUser() {
        UserActive userActive = new UserActive();
        userActive.setUsername("Taylor Swift");
        userActive.setPassword("08123456");
        userActive.setEmail("email@email.com");
        userActive.setState(true);

        userActiveRepository.save(userActive);
    }
}
