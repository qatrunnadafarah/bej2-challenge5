package org.binar.chapter4.repository;

import org.binar.chapter4.model.Seat;
import org.binar.chapter4.model.UserActive;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SeatRepositoryTest {

    @Autowired
    SeatRepository seatRepository;

    @Test
    void testAddSeat() {
        Seat seat = new Seat();
        seat.setStudioName("A");

        seatRepository.save(seat);
    }
}
