package org.binar.chapter4.repository;

import org.binar.chapter4.model.Schedule;
import org.binar.chapter4.model.UserActive;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ScheduleRepositoryTest {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Test
    void testAddSchedule() {
        Schedule schedule = new Schedule();
        schedule.setPrice(50000L);

        scheduleRepository.save(schedule);
    }

}
